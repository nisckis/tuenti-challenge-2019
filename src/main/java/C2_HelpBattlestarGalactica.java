import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

public class C2_HelpBattlestarGalactica {

    private static String fileName = "C2_HelpBattlestarGalactica-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;
    private static final String GALACTICA = "Galactica";
    private static final String NEW_EARTH = "New Earth";

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C2_HelpBattlestarGalactica().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int numPlanets = in.nextInt();
        in.nextLine();
        MutableGraph<String> spaceMap = GraphBuilder.directed().expectedNodeCount(numPlanets + 1).build();
        for (int numPlanet = 0; numPlanet < numPlanets; numPlanet++) {
            String planetHyperjumps = in.nextLine();
            String[] planetJumpMap = planetHyperjumps.split("[:,]");
            String sourcePlanet = planetJumpMap[0];
            spaceMap.addNode(sourcePlanet);
            for (int numJump = 1; numJump < planetJumpMap.length; numJump++) {
                spaceMap.putEdge(sourcePlanet, planetJumpMap[numJump]);
            }
        }
        LinkedList<String> states = new LinkedList<>();
        states.add(GALACTICA);
        long numPossibilities = 0L;
        while (!states.isEmpty()) {
            String currentPlanet = states.pollFirst();
            if (NEW_EARTH.equals(currentPlanet)) {
                numPossibilities++;
                continue;
            }
            Set<String> successors = spaceMap.successors(currentPlanet);
            states.addAll(successors);
        }
        out.print(numPossibilities);
    }
}
