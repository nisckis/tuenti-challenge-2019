import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class C1_OnionWars {

    private static String fileName = "C1_OnionWars-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C1_OnionWars().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int goodTaste = in.nextInt();
        int badTaste = in.nextInt();
        in.nextLine();
        if ((goodTaste & 1) == 1) {
            goodTaste++;
        }
        if ((badTaste & 1) == 1) {
            badTaste++;
        }
        int numTortillas = (goodTaste >> 1) + (badTaste >> 1);
        out.print(numTortillas);
    }
}
