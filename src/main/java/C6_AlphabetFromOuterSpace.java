import com.google.common.collect.Sets;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.MutableGraph;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class C6_AlphabetFromOuterSpace {

    private static String fileName = "C6_AlphabetFromOuterSpace-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;
    private static final String AMBIGUOUS = "AMBIGUOUS";

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C6_AlphabetFromOuterSpace().solve();
            out.println();
            out.flush();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int M = in.nextInt();
        in.nextLine();
        LinkedList<String> words = new LinkedList<>();
        HashSet<Integer> letters = Sets.newHashSetWithExpectedSize(256);
        for (int numWord = 0; numWord < M; numWord++) {
            String word = in.nextLine();
            letters.addAll(word.chars().boxed().collect(Collectors.toSet()));
            words.add(word);
        }
        if (M == 1) {
            if (letters.size() == 1) {
                out.print(words.getFirst());
            } else {
                out.print(AMBIGUOUS);
            }
            return;
        }
        MutableGraph<Character> alphabetMap = GraphBuilder.directed().expectedNodeCount(256).build();
        while (!words.isEmpty()) {
            String last = words.pollLast();
            String previous = words.peekLast();
            if (previous == null) {
                break;
            }
            int length = Math.min(last.length(), previous.length());
            for (int pos = 0; pos < length; pos++) {
                if (last.charAt(pos) != previous.charAt(pos)) {
                    alphabetMap.putEdge(previous.charAt(pos), last.charAt(pos));
                    break;
                }
            }
        }
        Set<Character> nodes = alphabetMap.nodes();
        if (nodes.size() != letters.size()) {
            out.print(AMBIGUOUS);
            return;
        }
        Set<Character> parents = nodes.parallelStream()
                .filter(node -> alphabetMap.predecessors(node).isEmpty())
                .collect(Collectors.toSet());
        if (parents.size() > 1) {
            out.print(AMBIGUOUS);
            return;
        }
        Set<Character> leaves = nodes.parallelStream()
                .filter(node -> alphabetMap.successors(node).isEmpty())
                .collect(Collectors.toSet());
        if (leaves.size() > 1) {
            out.print(AMBIGUOUS);
            return;
        }
        Character parent = parents.iterator().next();
        Character leaf = leaves.iterator().next();
        Set<String> paths = new LinkedHashSet<>();
        Queue<String> queuedNodes = new ArrayDeque<>();
        paths.add(parent.toString());
        queuedNodes.add(parent.toString());
        while (!queuedNodes.isEmpty()) {
            String currentPath = queuedNodes.remove();
            Character currentNode = currentPath.charAt(currentPath.length() - 1);
            for (Character successor : alphabetMap.successors(currentNode)) {
                if ((successor == leaf) && (currentPath.length() + 1 == letters.size())) {
                    // FOUND COMPLETE PATH
                    for (char letter : currentPath.toCharArray()) {
                        out.print(letter);
                        out.print(" ");
                    }
                    out.print(leaf);
                    return;
                }
                String newPath = currentPath + successor;
                if (paths.add(newPath)) {
                    queuedNodes.add(newPath);
                }
            }
        }
        out.print(AMBIGUOUS);
    }
}
