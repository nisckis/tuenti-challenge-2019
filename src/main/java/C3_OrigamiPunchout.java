import com.google.common.collect.HashBasedTable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class C3_OrigamiPunchout {

    private static String fileName = "C3_OrigamiPunchout-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;
    private static final char PUNCHED = 'O';

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ":");
            new C3_OrigamiPunchout().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int W = in.nextInt();
        int H = in.nextInt();
        int F = in.nextInt();
        int P = in.nextInt();
        in.nextLine();
        int fullWidth = W;
        int fullHeight = H;
        String firstHeightFold = null;
        String firstWidthFold = null;
        char firstFold = 0;
        for (int numFold = 0; numFold < F; numFold++) {
            String fold = in.nextLine();
            if (firstFold == 0) {
                firstFold = fold.charAt(0);
            }
            switch (fold) {
                case "L":
                case "R":
                    fullWidth <<= 1;
                    if (firstWidthFold == null) {
                        firstWidthFold = fold;
                    }
                    break;
                case "T":
                case "B":
                    fullHeight <<= 1;
                    if (firstHeightFold == null) {
                        firstHeightFold = fold;
                    }
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + fold);
            }
        }
        HashBasedTable<Integer, Integer, Character> foldedA = HashBasedTable.create(P, P);
        HashBasedTable<Integer, Integer, Character> foldedB = HashBasedTable.create(P, P);
        HashBasedTable<Integer, Integer, Character> foldedC = HashBasedTable.create(P, P);
        HashBasedTable<Integer, Integer, Character> foldedD = HashBasedTable.create(P, P);
        for (int numPunch = 0; numPunch < P; numPunch++) {
            int x = in.nextInt();
            int y = in.nextInt();
            in.nextLine();
            foldedA.put(y, x, PUNCHED);
            foldedB.put(H - 1 - y, x, PUNCHED);
            foldedC.put(H - 1 - y, W - 1 - x, PUNCHED);
            foldedD.put(y, W - 1 - x, PUNCHED);
        }
        HashBasedTable<Integer, Integer, Character> unfolded = HashBasedTable.create(P << F, P << F);
        for (int by = 0; by < (fullHeight / H); by += 2) {
            for (int bx = 0; bx < (fullWidth / W); bx += 2) {
                switch (firstFold) {
                    case 'B':
                        if ("R".equals(firstWidthFold)) {
                            // AD
                            // BC
                            copyValues(unfolded, foldedA, foldedD, foldedB, foldedC, bx, by, W, H);
                        } else if ("L".equals(firstWidthFold)) {
                            // DA
                            // CB
                            copyValues(unfolded, foldedD, foldedA, foldedC, foldedB, bx, by, W, H);
                        } else {
                            // NO WIDTH FOLD
                            // A
                            // B
                            copyValuesNoWidth(unfolded, foldedA, foldedB, bx, by, W, H);
                        }
                        break;
                    case 'T':
                        if ("R".equals(firstWidthFold)) {
                            // BC
                            // AD
                            copyValues(unfolded, foldedB, foldedC, foldedA, foldedD, bx, by, W, H);
                        } else if ("L".equals(firstWidthFold)) {
                            // CB
                            // DA
                            copyValues(unfolded, foldedC, foldedB, foldedD, foldedA, bx, by, W, H);
                        } else {
                            // NO WIDTH FOLD
                            // B
                            // A
                            copyValuesNoWidth(unfolded, foldedB, foldedA, bx, by, W, H);
                        }
                        break;
                    case 'R':
                        if ("B".equals(firstHeightFold)) {
                            // AD
                            // BC
                            copyValues(unfolded, foldedA, foldedD, foldedB, foldedC, bx, by, W, H);
                        } else if ("T".equals(firstHeightFold)) {
                            // BC
                            // AD
                            copyValues(unfolded, foldedB, foldedC, foldedA, foldedD, bx, by, W, H);
                        } else {
                            // NO HEIGHT FOLD
                            // AD
                            copyValuesNoHeight(unfolded, foldedA, foldedD, bx, by, W, H);
                        }
                        break;
                    case 'L':
                        if ("B".equals(firstHeightFold)) {
                            // DA
                            // CB
                            copyValues(unfolded, foldedD, foldedA, foldedC, foldedB, bx, by, W, H);
                        } else if ("T".equals(firstHeightFold)) {
                            // CB
                            // DA
                            copyValues(unfolded, foldedC, foldedB, foldedD, foldedA, bx, by, W, H);
                        } else {
                            // NO HEIGHT FOLD
                            // DA
                            copyValuesNoHeight(unfolded, foldedD, foldedA, bx, by, W, H);
                        }
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + firstFold);
                }
            }
        }
        unfolded.columnKeySet().stream().sorted().forEach(column -> unfolded.column(column).keySet().stream().sorted().forEach(row -> {
            out.println();
            out.print(column);
            out.print(" ");
            out.print(row);
        }));
        out.flush();
    }

    private static void copyValuesNoHeight(HashBasedTable<Integer, Integer, Character> unfolded, HashBasedTable<Integer, Integer, Character> folded00, HashBasedTable<Integer, Integer, Character> folded01, int bx, int by, int W, int H) {
        folded00.cellSet().forEach(cell -> unfolded.put((by * H) + cell.getRowKey(), (bx * W) + cell.getColumnKey(), PUNCHED));
        folded01.cellSet().forEach(cell -> unfolded.put((by * H) + cell.getRowKey(), ((bx + 1) * W) + cell.getColumnKey(), PUNCHED));
    }

    private static void copyValuesNoWidth(HashBasedTable<Integer, Integer, Character> unfolded, HashBasedTable<Integer, Integer, Character> folded00, HashBasedTable<Integer, Integer, Character> folded10, int bx, int by, int W, int H) {
        folded00.cellSet().forEach(cell -> unfolded.put((by * H) + cell.getRowKey(), (bx * W) + cell.getColumnKey(), PUNCHED));
        folded10.cellSet().forEach(cell -> unfolded.put(((by + 1) * H) + cell.getRowKey(), (bx * W) + cell.getColumnKey(), PUNCHED));
    }

    private static void copyValues(HashBasedTable<Integer, Integer, Character> unfolded, HashBasedTable<Integer, Integer, Character> folded00, HashBasedTable<Integer, Integer, Character> folded01, HashBasedTable<Integer, Integer, Character> folded10, HashBasedTable<Integer, Integer, Character> folded11, int bx, int by, int W, int H) {
        copyValuesNoHeight(unfolded, folded00, folded01, bx, by, W, H);
        copyValuesNoHeight(unfolded, folded10, folded11, bx, by + 1, W, H);
    }
}
