import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import sun.plugin.dom.exception.InvalidStateException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.*;

public class C9_HelpingNobita {

    private static String fileName = "C9_HelpingNobita-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static String databaseFileName = "C9_HelpingNobita.db";
    private static Scanner in;
    private static PrintWriter out;
    private static final String ONE = "一";
    private static final String TWO = "二";
    private static final String THREE = "三";
    private static final String FOUR = "四";
    private static final String FIVE = "五";
    private static final String SIX = "六";
    private static final String SEVEN = "七";
    private static final String EIGHT = "八";
    private static final String NINE = "九";
    private static final String TEN = "十";
    private static final String HUNDRED = "百";
    private static final String THOUSAND = "千";
    private static final String TEN_THOUSAND = "万";
    private static final BiMap<Integer, String> WESTERN_TO_KANJI = HashBiMap.create(12);
    private static final Multimap<String, Integer> SUFFLED_KANJI_TO_WESTERN = HashMultimap.create();

    public static final class Database {
        public static void main(String[] args) throws FileNotFoundException {
            fillKanjiToWestern();
            PrintWriter database = new PrintWriter(databaseFileName);
            for (int number = 1; number < 100000; number++) {
                database.println(convert(number));
            }
            database.close();
        }

        private static String convert(int convert_num) {
            if (convert_num < 10) {
                return len_one(convert_num);
            }
            if (convert_num < 100) {
                return len_two(convert_num);
            }
            if (convert_num < 1_000) {
                return len_three(convert_num);
            }
            if (convert_num < 10_000) {
                return len_four(convert_num);
            }
            return len_five(convert_num);
        }

        private static String len_one(int convert_num) {
            return WESTERN_TO_KANJI.get(convert_num);
        }

        private static String len_two(int convert_num) {
            if (convert_num == 10) {
                // Exception, if number is 10, simple return 10
                return TEN;
            }
            if ((convert_num / 10) == 1) {
                //When first number is 1, use ten plus second number
                return TEN + WESTERN_TO_KANJI.get(convert_num % 10);
            }
            if (convert_num % 10 == 0) {
                // If ending number is zero, give first number plus 10
                return WESTERN_TO_KANJI.get(convert_num / 10) + TEN;
            }
            return WESTERN_TO_KANJI.get(convert_num / 10) + TEN + WESTERN_TO_KANJI.get(convert_num % 10);
        }

        private static String len_three(int convert_num) {
            if (convert_num == 100) {
                return HUNDRED;
            }
            ArrayList<String> num_list = new ArrayList<>(6);
            if (convert_num / 100 == 1) {
                num_list.add(HUNDRED);
            } else {
                num_list.add(WESTERN_TO_KANJI.get(convert_num / 100));
                num_list.add(HUNDRED);
            }
            if (convert_num % 100 != 0) {
                num_list.add(convert(convert_num % 100));
            }
            return String.join("", num_list);
        }

        private static String len_four(int convert_num) {
            if (convert_num == 1000) {
                return THOUSAND;
            }
            ArrayList<String> num_list = new ArrayList<>(8);
            if (convert_num / 1000 == 1) {
                num_list.add(THOUSAND);
            } else {
                num_list.add(WESTERN_TO_KANJI.get(convert_num / 1000));
                num_list.add(THOUSAND);
            }
            if (convert_num % 1000 != 0) {
                num_list.add(convert(convert_num % 1000));
            }
            return String.join("", num_list);
        }

        private static String len_five(int convert_num) {
            if (convert_num == 10000) {
                return ONE + TEN_THOUSAND;
            }
            ArrayList<String> num_list = new ArrayList<>(8);
            num_list.add(WESTERN_TO_KANJI.get(convert_num / 10000));
            num_list.add(TEN_THOUSAND);
            num_list.add(convert(convert_num % 10000));
            return String.join("", num_list);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        fillKanjiToWestern();
        loadDatabase();
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C9_HelpingNobita().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        String operand1 = in.next();
        if (operand1.length() > 10) {
            throw new InvalidStateException("Unexpected operand 1 length " + operand1.length());
        }
        in.next();
        String operand2 = in.next();
        if (operand2.length() > 10) {
            throw new InvalidStateException("Unexpected operand 2 length " + operand2.length());
        }
        in.next();
        String result = in.next();
        if (result.length() > 10) {
            throw new InvalidStateException("Unexpected result length " + result.length());
        }
        in.nextLine();
        String[] characters = operand1.split("");
        Arrays.sort(characters);
        Collection<Integer> firstOperandNumbers = SUFFLED_KANJI_TO_WESTERN.get(String.join("", characters));
        characters = operand2.split("");
        Arrays.sort(characters);
        Collection<Integer> secondOperandNumbers = SUFFLED_KANJI_TO_WESTERN.get(String.join("", characters));
        characters = result.split("");
        Arrays.sort(characters);
        Collection<Integer> resultNumbers = SUFFLED_KANJI_TO_WESTERN.get(String.join("", characters));
        for (Integer firstOperandNumber : firstOperandNumbers) {
            for (Integer secondOperandNumber : secondOperandNumbers) {
                for (Integer resultNumber : resultNumbers) {
                    if (firstOperandNumber + secondOperandNumber == resultNumber) {
                        out.print(firstOperandNumber);
                        out.print(" + ");
                        out.print(secondOperandNumber);
                        out.print(" = ");
                        out.print(resultNumber);
                        return;
                    }
                    if (firstOperandNumber - secondOperandNumber == resultNumber) {
                        out.print(firstOperandNumber);
                        out.print(" - ");
                        out.print(secondOperandNumber);
                        out.print(" = ");
                        out.print(resultNumber);
                        return;
                    }
                    if ((long)firstOperandNumber * (long)secondOperandNumber == (long)resultNumber) {
                        out.print(firstOperandNumber);
                        out.print(" * ");
                        out.print(secondOperandNumber);
                        out.print(" = ");
                        out.print(resultNumber);
                        return;
                    }
                }
            }
        }
        out.print(operand1);
        out.print(" OPERAND ");
        out.print(operand2);
        out.print(" = ");
        out.print(result);
        out.flush();
//        throw new InvalidStateException("Could not compute values");
    }

    private static void loadDatabase() throws FileNotFoundException {
        Scanner database = new Scanner(new FileReader(databaseFileName));
        for (int number = 1; number < 100000; number++) {
            String[] splitNumber = database.nextLine().split("");
            Arrays.sort(splitNumber);
            SUFFLED_KANJI_TO_WESTERN.put(String.join("", splitNumber), number);
        }
    }

    private static void fillKanjiToWestern() {
        WESTERN_TO_KANJI.put(1, ONE);
        WESTERN_TO_KANJI.put(2, TWO);
        WESTERN_TO_KANJI.put(3, THREE);
        WESTERN_TO_KANJI.put(4, FOUR);
        WESTERN_TO_KANJI.put(5, FIVE);
        WESTERN_TO_KANJI.put(6, SIX);
        WESTERN_TO_KANJI.put(7, SEVEN);
        WESTERN_TO_KANJI.put(8, EIGHT);
        WESTERN_TO_KANJI.put(9, NINE);
        WESTERN_TO_KANJI.put(10, TEN);
        WESTERN_TO_KANJI.put(100, HUNDRED);
        WESTERN_TO_KANJI.put(1000, THOUSAND);
        WESTERN_TO_KANJI.put(10000, TEN_THOUSAND);
    }
}
