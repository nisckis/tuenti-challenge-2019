import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class C5_ForbiddenLove {

    private static String fileName = "C5_ForbiddenLove-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;
    private static String LETTERS = "1234567890QWERTYUIOPASDFGHJKL;ZXCVBNM,.-";
    private static char[] TYPEWRITER = LETTERS.toCharArray();
    private static int B_POS = LETTERS.indexOf('B');
    private static int B_POS_X = B_POS % 10;
    private static int B_POS_Y = B_POS / 10;
    private static int G_POS = LETTERS.indexOf('G');
    private static int G_POS_X = G_POS % 10;
    private static int G_POS_Y = G_POS / 10;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C5_ForbiddenLove().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        char sender = in.nextLine().charAt(0);
        char[] encryptedMessage = in.nextLine().toCharArray();
        char lastLetter = encryptedMessage[encryptedMessage.length - 1];
        int displacementX;
        int displacementY;
        int posLast = LETTERS.indexOf(lastLetter);
        switch (sender) {
            case 'B':
                displacementX = (B_POS_X - (posLast % 10) + 10) % 10;
                displacementY = (B_POS_Y - (posLast / 10) + 4) % 4;
                break;
            case 'G':
                displacementX = (G_POS_X - (posLast % 10) + 10) % 10;
                displacementY = (G_POS_Y - (posLast / 10) + 4) % 4;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sender);
        }
        char[] message = new char[encryptedMessage.length];
        for (int pos = 0; pos < encryptedMessage.length; pos++) {
            if (encryptedMessage[pos] == ' ') {
                message[pos] = ' ';
                continue;
            }
            int posLetter = LETTERS.indexOf(encryptedMessage[pos]);
            int posLetterX = ((posLetter % 10) + displacementX + 10) % 10;
            int posLetterY = ((posLetter / 10) + displacementY + 4) % 4;
            message[pos] = TYPEWRITER[((posLetterY * 10) + posLetterX + TYPEWRITER.length) % TYPEWRITER.length];
        }
        out.print(message);
    }
}
