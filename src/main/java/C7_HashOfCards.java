import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class C7_HashOfCards {

    private static String fileName = "C7_HashOfCards-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C7_HashOfCards().solve();
            out.println();
            out.flush();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int M = in.nextInt();
        in.nextLine();
        StringBuilder cardBuffer = new StringBuilder(1024 * 1024);
        for (int line = 0; line < M; line++) {
            cardBuffer.append(in.nextLine());
        }
        byte[] originalCardHash = notSoComplexHash(cardBuffer.toString());
        int L = in.nextInt();
        in.nextLine();
        cardBuffer.setLength(0);
        for (int line = 0; line < L; line++) {
            cardBuffer.append(in.nextLine());
        }
        String newCardBuffer = cardBuffer.toString();
        byte[] newCardHash = notSoComplexHash(newCardBuffer);
        if (Arrays.equals(originalCardHash, newCardHash)) {
            // NEW CARD ALREADY FINE
            return;
        }
        String[] split = newCardBuffer.split("------");
        String preamble = split[0] + "---";
        String body = "---" + split[1];
        rotateArray(originalCardHash, preamble.length());
        solutionLength:
        for (int solutionLength = 1; solutionLength <= 64; solutionLength++) {
            String newCard = preamble + String.valueOf(new char[solutionLength]) + body;
            newCardHash = notSoComplexHash(newCard);
            rotateArray(newCardHash, preamble.length());
            // ONLY ONE ROW
            if (solutionLength <= 16) {
                for (int position = solutionLength; position < 16; position++) {
                    if (originalCardHash[position] != 0) {
                        // NOT A VALID LENGTH SOLUTION
                        continue solutionLength;
                    }
                }
                StringBuilder solution = new StringBuilder(16);
                for (int position = 0; position < solutionLength; position++) {
                    int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                    if ((diff < 48) || (diff > 122)) {
                        // NOT A VALID LENGTH SOLUTION
                        continue solutionLength;
                    }
                    solution.append((char) diff);
                }
                out.print(solution.toString());
                return;
            }
            // AT MOST TWO ROWS
            if (solutionLength <= 32) {
                StringBuilder solution = new StringBuilder(16);
                StringBuilder solution2 = new StringBuilder(16);
                for (int position = 0; position < 16; position++) {
                    if (position <= ((solutionLength - 1) % 16)) {
                        // COMPUTE 2 CHARS
                        int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                        if ((diff < 48 * 2) || (diff > 122 * 2)) {
                            // NOT A VALID LENGTH SOLUTION
                            continue solutionLength;
                        }
                        if (diff <= 48 + 122) {
                            diff = diff - 48;
                            solution.append("0");
                            solution2.append((char) diff);
                            continue;
                        }
                        // (diff <= 122 * 2)
                        diff = diff - 122;
                        solution.append((char) diff);
                        solution2.append("z");
                    } else {
                        // COMPUTE 1 CHAR
                        int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                        if ((diff < 48) || (diff > 122)) {
                            // NOT A VALID LENGTH SOLUTION
                            continue solutionLength;
                        }
                        solution.append((char) diff);
                    }
                }
                out.print(solution.toString());
                out.print(solution2.toString());
                return;
            }
            // AT MOST THREE ROWS
            if (solutionLength <= 48) {
                StringBuilder solution = new StringBuilder(16);
                StringBuilder solution2 = new StringBuilder(16);
                StringBuilder solution3 = new StringBuilder(16);
                for (int position = 0; position < 16; position++) {
                    if (position <= ((solutionLength - 1) % 16)) {
                        // COMPUTE 3 CHARS
                        int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                        if (diff < 48 * 3) {
                            diff += 256;
                        }
                        if (diff <= 48 + 48 + 122) {
                            diff = diff - 48 - 48;
                            solution.append("0");
                            solution2.append("0");
                            solution3.append((char) diff);
                            continue;
                        }
                        if (diff <= 48 + 122 + 122) {
                            diff = diff - 48 - 122;
                            solution.append("0");
                            solution2.append((char) diff);
                            solution3.append("z");
                            continue;
                        }
                        if (diff <= 122 * 3) {
                            diff = diff - 122 - 122;
                            solution.append((char) diff);
                            solution2.append("z");
                            solution3.append("z");
                            continue;
                        }
                        // NOT A VALID LENGTH SOLUTION
                        continue solutionLength;
                    } else {
                        // COMPUTE 2 CHARS
                        int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                        if ((diff < 48 * 2) || (diff > 122 * 2)) {
                            // NOT A VALID LENGTH SOLUTION
                            continue solutionLength;
                        }
                        if (diff <= 48 + 122) {
                            diff = diff - 48;
                            solution.append("0");
                            solution2.append((char) diff);
                            continue;
                        }
                        // (diff <= 122 * 2)
                        diff = diff - 122;
                        solution.append((char) diff);
                        solution2.append("z");
                    }
                }
                out.print(solution.toString());
                out.print(solution2.toString());
                out.print(solution3.toString());
                return;
            }
            // THERE ARE FOUR ROWS
            StringBuilder solution = new StringBuilder(16);
            StringBuilder solution2 = new StringBuilder(16);
            StringBuilder solution3 = new StringBuilder(16);
            StringBuilder solution4 = new StringBuilder(16);
            fourRowsSolution:
            for (int position = 0; position < 16; position++) {
                if (position <= ((solutionLength - 1) % 16)) {
                    // COMPUTE 4 CHARS THIS ALWAYS HAS A SOLUTION
                    int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                    for (char char1 = '0'; char1 <= 'z'; char1++) {
                        for (char char2 = '0'; char2 <= 'z'; char2++) {
                            for (char char3 = '0'; char3 <= 'z'; char3++) {
                                for (char char4 = '0'; char4 <= 'z'; char4++) {
                                    if (diff == ((char1 + char2 + char3 + char4 + 256) % 256)) {
                                        solution.append(char1);
                                        solution2.append(char2);
                                        solution3.append(char3);
                                        solution4.append(char4);
                                        continue fourRowsSolution;
                                    }
                                }
                            }
                        }
                    }
                    throw new IllegalStateException("Unexpected value: " + diff + " at solutionLength:" + solutionLength);
                } else {
                    // COMPUTE 3 CHARS
                    int diff = (originalCardHash[position] - newCardHash[position] + 256) % 256;
                    if (diff < 48 * 3) {
                        diff += 256;
                    }
                    if (diff <= 48 + 48 + 122) {
                        diff = diff - 48 - 48;
                        solution.append("0");
                        solution2.append("0");
                        solution3.append((char) diff);
                        continue;
                    }
                    if (diff <= 48 + 122 + 122) {
                        diff = diff - 48 - 122;
                        solution.append("0");
                        solution2.append((char) diff);
                        solution3.append("z");
                        continue;
                    }
                    if (diff <= 122 * 3) {
                        diff = diff - 122 - 122;
                        solution.append((char) diff);
                        solution2.append("z");
                        solution3.append("z");
                        continue;
                    }
                    // NOT A VALID LENGTH SOLUTION
                    continue solutionLength;
                }
            }
            out.print(solution.toString());
            out.print(solution2.toString());
            out.print(solution3.toString());
            out.print(solution4.toString());
            return;
        }
        throw new IllegalStateException("NO SOLUTION FOUND");
    }

    private void rotateArray(byte[] originalCardHash, int length) {
        byte[] tmpBuff = new byte[16];
        for (int pos = 0; pos < 16; pos++) {
            tmpBuff[pos] = originalCardHash[(pos + length) % 16];
        }
        System.arraycopy(tmpBuff, 0, originalCardHash, 0, 16);
    }

    private static byte[] notSoComplexHash(String inputText) {
        byte[] hash = new byte[16];
        Arrays.fill(hash, (byte) 0x00);
        byte[] textBytes = inputText.getBytes(StandardCharsets.ISO_8859_1);
        for (int i = 0; i < textBytes.length; i++) {
            hash[i % 16] = (byte) (hash[i % 16] + textBytes[i]);
        }
        return hash;
    }
}
