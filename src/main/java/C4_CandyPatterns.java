import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;

public class C4_CandyPatterns {

    private static String fileName = "C4_CandyPatterns-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new C4_CandyPatterns().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int N = in.nextInt();
        in.nextLine();
        TreeMap<Integer, BigInteger> entries = new TreeMap<>();
        for (int numEntry = 0; numEntry < N; numEntry++) {
            entries.merge(in.nextInt(), BigInteger.ONE, BigInteger::add);
        }
        in.nextLine();
        // COMPUTE MINIMAL LIST CLONING FOR GETTING ORIGINAL LIST
        entries.descendingMap().forEach((value, count) -> {
            BigInteger lcm = lcm(value, count);
            BigInteger multiplier = lcm.divide(count);
            if (multiplier.compareTo(BigInteger.ONE) > 0) {
                entries.forEach((oldValue, oldCount) -> entries.put(oldValue, oldCount.multiply(multiplier)));
            }
        });
        AtomicReference<BigInteger> numCandies = new AtomicReference<>(BigInteger.ZERO);
        AtomicReference<BigInteger> numAttendees = new AtomicReference<>(BigInteger.ZERO);
        entries.forEach((value, count) -> {
//            numCandies.addAndGet(count);
            numCandies.accumulateAndGet(count, BigInteger::add);
            numAttendees.accumulateAndGet(count.divide(BigInteger.valueOf(value)), BigInteger::add);
        });
        BigInteger gcd = numCandies.get().gcd(numAttendees.get());
        out.print(numCandies.get().divide(gcd));
        out.print("/");
        out.print(numAttendees.get().divide(gcd));
    }

    private static int gcd(int a, int b) {
        while (b > 0) {
            int temp = b;
            b = a % b; // % is remainder
            a = temp;
        }
        return a;
    }

    private static BigInteger lcm(int a, BigInteger b) {
        BigInteger bigA = BigInteger.valueOf(a);
        return bigA.multiply(b.divide(b.gcd(bigA)));
    }
}
